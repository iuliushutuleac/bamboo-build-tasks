/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.chain;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plan.PlanExecutionManager;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.Maps;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class ChainedBuildTaskConfigurator extends AbstractTaskConfigurator
{

    private CachedPlanManager cachedPlanManager;
    private PlanExecutionManager planExecutionManager;

    public CachedPlanManager getCachedPlanManager() {
        return cachedPlanManager;
    }

    public void setCachedPlanManager(CachedPlanManager cachedPlanManager) {
        this.cachedPlanManager = cachedPlanManager;
    }

    public PlanExecutionManager getPlanExecutionManager() {
        return planExecutionManager;
    }

    public void setPlanExecutionManager(PlanExecutionManager planExecutionManager) {
        this.planExecutionManager = planExecutionManager;
    }

    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put(Constants.BUILD_PLUGIN_KEY, params.getString(Constants.BUILD_PLUGIN_KEY));
        config.put(Constants.BUILD_BRANCH_NAME, params.getString(Constants.BUILD_BRANCH_NAME));
        config.put(Constants.BUILD_VARIABLES_NAME, params.getString(Constants.BUILD_VARIABLES_NAME));
        config.put(Constants.BUILD_REVISION_NAME, params.getString(Constants.BUILD_REVISION_NAME));

        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);
        Map<String, String> result = Maps.newLinkedHashMap();
        if (cachedPlanManager != null)
        {
            for (ImmutableTopLevelPlan p : cachedPlanManager.getPlans())
            {

                result.put(p.getName(), p.getKey());

            }
        }

        context.put("keys", result);
        
    }
    
    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);
        Map<String, String> result = Maps.newLinkedHashMap();
        if (cachedPlanManager != null)
        {
            for (ImmutableTopLevelPlan p : cachedPlanManager.getPlans())
            {

                result.put(p.getName(), p.getKey());

            }
        }
        context.put(Constants.BUILD_PLUGIN_KEY, taskDefinition.getConfiguration().get(Constants.BUILD_PLUGIN_KEY));
        context.put(Constants.BUILD_BRANCH_NAME, taskDefinition.getConfiguration().get(Constants.BUILD_BRANCH_NAME));
        context.put(Constants.BUILD_VARIABLES_NAME, taskDefinition.getConfiguration().get(Constants.BUILD_VARIABLES_NAME));
        context.put(Constants.BUILD_REVISION_NAME, taskDefinition.getConfiguration().get(Constants.BUILD_REVISION_NAME));
        context.put("keys", result);

    }


    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);
    }

}
