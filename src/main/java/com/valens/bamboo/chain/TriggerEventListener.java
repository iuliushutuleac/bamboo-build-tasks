package com.valens.bamboo.chain;


import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.deployments.execution.events.DeploymentFinishedEvent;
import com.atlassian.bamboo.deployments.execution.events.DeploymentStartedEvent;
import com.atlassian.bamboo.event.BuildCompletedEvent;
import com.atlassian.bamboo.event.JobCompletedEvent;
import com.atlassian.bamboo.plan.*;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.plan.cache.ImmutableChainBranch;
import com.atlassian.bamboo.user.BambooUser;
import com.atlassian.bamboo.user.DefaultBambooUser;
import com.atlassian.bamboo.util.AcquisitionPolicy;
import com.atlassian.bamboo.util.RequestCacheThreadLocal;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.events.BuildQueuedEvent;
import com.atlassian.bamboo.v2.build.trigger.ManualBuildTriggerReason;
import com.atlassian.bamboo.v2.trigger.DependencyChainListener;
import com.atlassian.bamboo.v2.trigger.ManualBuildDetectionAction;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.event.Event;
import com.atlassian.event.api.EventListener;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginUninstalledEvent;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

public class TriggerEventListener {
    private CachedPlanManager cachedPlanManager;
    private PlanExecutionManager planExecutionManager;
    BandanaManager bandanaManager;

    private static final Logger log = Logger.getLogger(ChainPlanTrigger.class);
    static boolean running;
    static int k = 0;

    public BandanaManager getBandanaManager() {
        return bandanaManager;
    }

    public void setBandanaManager(BandanaManager bandanaManager) {
        this.bandanaManager = bandanaManager;
    }

    public CachedPlanManager getCachedPlanManager() {
        return cachedPlanManager;
    }

    public void setCachedPlanManager(CachedPlanManager cachedPlanManager) {
        this.cachedPlanManager = cachedPlanManager;
    }

    public PlanExecutionManager getPlanExecutionManager() {

        return planExecutionManager;
    }

    public void setPlanExecutionManager(PlanExecutionManager planExecutionManager) {
        this.planExecutionManager = planExecutionManager;
    }

    protected Map<String, String> calculateParams(String revision) {
        Map<String, String> params = new HashMap<String, String>();
        if(revision != null) {
            log.info("CUSTOM_REVISION_PARAMETER " + revision);
            params.put(ManualBuildDetectionAction.CUSTOM_REVISION_PARAMETER, revision);
            params.put(DependencyChainListener.DEPENDENCIES_DISABLED, Boolean.TRUE.toString());
        }
        else
            params.put(DependencyChainListener.DEPENDENCIES_DISABLED, Boolean.toString(false));

        return params;
    }

    @EventListener
    public void onBuildQueuedEvent(TriggeringEvent event) {
        handleEvent(event);
    }

    public void handleEvent(TriggeringEvent event) {

                synchronized(this) {

                    HashMap<String, String> data = (HashMap<String, String>) event.getSource();
                    try {

                        String key = data.get(Constants.BUILD_PLUGIN_KEY);
                        String branchName = data.get(Constants.BUILD_BRANCH_NAME);

                        log.info("CHAIN Starting build: " + key.split("=")[1]);

                        final ImmutableChain tempchain = ((CachedPlanManager) ContainerManager.getComponent("cachedPlanManager")).getPlanByKeyIfOfType(PlanKeys.getPlanKey(key.split("=")[1]), ImmutableChain.class);

                        List<ImmutableChainBranch> branches = ((CachedPlanManager) ContainerManager.getComponent("cachedPlanManager")).getBranchesForChain(tempchain);
                        ImmutableChain chain = tempchain;
                        if (branchName != null && branchName.trim().length() > 0)
                            for (ImmutableChainBranch b : branches) {
                                if (branchName.equalsIgnoreCase(b.getBuildName()))
                                    chain = b;
                                log.info(b.getBuildName());
                            }
                        PlanResultKey planResultKey = null;

                        if (chain != null) {
                            Map<String, String> vars = new HashMap<String, String>();
                            for (String g : data.keySet())
                                vars.put("trigger." + g, data.get(g));

                            try {
                                if(data.get(Constants.BUILD_VARIABLES_NAME) != null)
                                    for(String s :  data.get(Constants.BUILD_VARIABLES_NAME).split("\n"))
                                        vars.put(s.split("=")[0],s.split("=")[1]);
                            } catch (Exception e) {
                                log.info("Exception on variable split: " + e.getMessage());
                            }

                            Map<String, String> params = calculateParams(data.get(Constants.BUILD_REVISION_NAME));

                            ExecutionRequestResult result = planExecutionManager.start(chain,
                                    null,
                                    ManualBuildTriggerReason.KEY,
                                    "com.valens.bamboo.build.bamboo-build-task:chainPlanTrigger",
                                    params,
                                    vars,
                                    AcquisitionPolicy.IMMEDIATE);



                            planResultKey = result.getPlanResultKey();

                        }

                    } catch (Exception e) {
                        if (k > 0)
                            k--;
                        e.printStackTrace();
                    }
                }
    }

}
