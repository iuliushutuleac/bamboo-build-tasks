/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.valens.bamboo.chain;

/**
 *
 * @author IHutuleac
 */
public class Constants {
    public static final String PREFIX = "custom.bamboo.";
    public static final String BUILD_PLUGIN_KEY = PREFIX + "plan.key";    
    public static final String BUILD_BRANCH_NAME = PREFIX + "branchName";
    public static final String BUILD_VARIABLES_NAME = PREFIX + "variables";
    public static final String BUILD_REVISION_NAME = PREFIX + "revision";
}
