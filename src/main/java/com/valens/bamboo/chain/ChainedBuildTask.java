/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.chain;

import com.atlassian.bamboo.bandana.PlanAwareBandanaContext;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.spring.ComponentAccessor;
import com.atlassian.bamboo.task.*;
import com.atlassian.bamboo.v2.build.agent.remote.plugins.DeferredEventsHandler;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.spring.container.ContainerManager;
import org.apache.commons.lang.StringEscapeUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

public class ChainedBuildTask implements CommonTaskType
{

    EventPublisher eventPublisher;

    public EventPublisher getEventPublisher() {
        if (eventPublisher == null)
            eventPublisher = (EventPublisher) (ContainerManager
                    .getComponent("eventPublisher"));
        return eventPublisher;
    }
    public TaskResult execute(final CommonTaskContext taskContext) throws TaskException
    {
        try
        {

            String key = taskContext.getConfigurationMap().get(Constants.BUILD_PLUGIN_KEY);
            String branchName = taskContext.getConfigurationMap().get(Constants.BUILD_BRANCH_NAME);
            String revision = taskContext.getConfigurationMap().get(Constants.BUILD_REVISION_NAME);
            String variables = taskContext.getConfigurationMap().get(Constants.BUILD_VARIABLES_NAME);
            HashMap<String, String> data = new HashMap<>();
            for(String g: taskContext.getCommonContext().getVariableContext().getEffectiveVariables().keySet() )
                if(!g.contains("planRepository"))
                    data.put(g, taskContext.getCommonContext().getVariableContext().getEffectiveVariables().get(g).getValue());
            for(String g: taskContext.getRuntimeTaskContext().keySet() )
                data.put(g, taskContext.getRuntimeTaskContext().get(g));
            data.put("displayName", taskContext.getCommonContext().getDisplayName());
            data.put("entityKey", taskContext.getCommonContext().getEntityKey().toString());

            for(String g: taskContext.getConfigurationMap().keySet() )
                data.put(g, taskContext.getConfigurationMap().get(g));

            if (branchName.contains(",")) {
                for(String s : branchName.split(","))
                {
                    data.put(Constants.BUILD_BRANCH_NAME, s);
                    if(revision != null && revision.trim().length() > 0)
                        data.put(Constants.BUILD_REVISION_NAME, revision);
                    if(variables != null && variables.trim().length() > 0)
                        data.put(Constants.BUILD_VARIABLES_NAME, variables);

                    getEventPublisher().publish(new TriggeringEvent(data));
                }
            }
            else
                getEventPublisher().publish(new TriggeringEvent(data));

        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return TaskResultBuilder.newBuilder(taskContext).success().build();
    }

}
