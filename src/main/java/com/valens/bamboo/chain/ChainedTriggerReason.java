/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.chain;

import com.atlassian.bamboo.core.BambooCustomDataAware;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.v2.build.trigger.AbstractTriggerReason;
import java.util.Map;

/**
 *
 * @author IHutuleac
 */
public class ChainedTriggerReason extends AbstractTriggerReason
{

    public void init(String string, Map<String, String> map)
    {
        
    }

    public void init(String string, ResultsSummary rs)
    {
        
    }

    public void updateCustomData(BambooCustomDataAware bcda)
    {
        
    }

    public String getName()
    {
        return "Chained execution.";
    }

    public String getNameForSentence()
    {
        return "Chained execution.";
    }
    
}
