/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.chain;

import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.plan.trigger.PlanTrigger;
import com.atlassian.bamboo.plan.trigger.PlanTriggerResult;
import com.atlassian.bamboo.plan.trigger.PlanTriggerResultBuilder;
import com.atlassian.bamboo.plan.vcsRevision.PlanVcsRevisionDataSet;
import com.atlassian.bamboo.repository.RepositoryException;
import com.atlassian.bamboo.v2.build.BuildChanges;
import com.atlassian.bamboo.v2.build.trigger.TriggerReason;
import com.atlassian.bamboo.v2.trigger.ChangeDetectionManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
/**
 *
 * @author IHutuleac
 */

public class ChainPlanTrigger implements PlanTrigger
{
    private static final Logger log = Logger.getLogger(ChainPlanTrigger.class);

    private CachedPlanManager cachedPlanManager;
    private ChangeDetectionManager changeDetectionManager;

    public CachedPlanManager getCachedPlanManager() {
        return cachedPlanManager;
    }

    public void setCachedPlanManager(CachedPlanManager cachedPlanManager) {
        this.cachedPlanManager = cachedPlanManager;
    }

    public ChangeDetectionManager getChangeDetectionManager() {
        return changeDetectionManager;
    }

    public void setChangeDetectionManager(ChangeDetectionManager changeDetectionManager) {
        this.changeDetectionManager = changeDetectionManager;
    }

    public PlanTriggerResult triggerPlan(@NotNull final TriggerReason triggerReason,
                                         @NotNull final PlanResultKey planResultKey,
                                         @NotNull final Map<String, String> params,
                                         @NotNull final Map<String, String> customVariables,
                                         @Nullable final PlanVcsRevisionDataSet lastVcsRevisionKeys)
    {
        final PlanTriggerResultBuilder resultBuilder = PlanTriggerResultBuilder.create(customVariables);

        try
        {
            final ImmutableChain chain = cachedPlanManager.getPlanByKeyIfOfType(planResultKey.getPlanKey(), ImmutableChain.class);
            if (chain == null)
            {
                throw new IllegalStateException("Could not trigger a build with reason '" + triggerReason.getName() + "' because the plan '" + planResultKey.getPlanKey() + "' does not exist");
            }

            BuildChanges buildChanges = changeDetectionManager.collectAllChangesSinceLastBuild(chain, customVariables, null); 
            resultBuilder.addBuildChanges(buildChanges);
        }
        catch (RepositoryException e)
        {
            final String errorMessage = "Could not detect changes for plan '" + planResultKey + "' with trigger '" + triggerReason.getName() + "'";
            
            resultBuilder.addErrorMessage(errorMessage, e);
        }

        return resultBuilder.build();
    }

    public Map<String, String> getVariablesForContinuedBuild(TriggerReason tr)
    {
        return new HashMap<String, String>();
    }
    
}
