/*
 * Copyright (C) 2016 Valens Soft
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.valens.bamboo.chain;

import com.atlassian.event.Event;


/**
 *
 * @author IHutuleac
 */
public class TriggeringEvent extends Event
{
    private final Object source;

    public TriggeringEvent(Object source)
    {
        super(source);
        this.source = source;
    }
   

}
