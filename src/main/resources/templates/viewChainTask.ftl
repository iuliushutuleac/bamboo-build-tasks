[#if build.buildDefinition.customConfiguration.get('custom.bamboo.plan.key')?has_content ]
    [@ww.label label='Build Key' ]
        [@ww.param name='value']${build.buildDefinition.customConfiguration.get('custom.bamboo.plan.key')?if_exists}[/@ww.param]
    [/@ww.label]
    
    [@ww.label label="Branch" name="custom.bamboo.branchName" /]
    [@ww.label label="Variables" name="custom.bamboo.variables" /]
[/#if]