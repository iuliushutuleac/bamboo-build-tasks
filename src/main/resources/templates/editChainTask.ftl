[@ww.select name='custom.bamboo.plan.key' 
            label='Build Plans'  list=keys
            description='Build Plans List' emptyOption="true" cssClass="full-width-field" /]

[@ww.textfield label="Branch Name" name="custom.bamboo.branchName" required='false' cssClass="full-width-field" /]

[@ww.textarea label="Variables" name="custom.bamboo.variables" required='false' cssClass="full-width-field" rows="10" description='Multiple lines containing KEY=Value pairs' /]